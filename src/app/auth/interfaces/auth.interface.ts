export interface AuthRespuesta {
  ok: boolean,
  uid?: string,
  nombre?: string,
  token?: string,
  mensaje: string,
}

export interface user {
  uid: string;
  nombre: string;
}
