import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthRespuesta, user } from '../interfaces/auth.interface';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private Baseurl: string = environment.url
  private _usuario: user;

  get usuario(){
    return {...this._usuario}
  }

  constructor(private http:HttpClient) { }

  login(email:string, contrasena:string){
    const url = `${this.Baseurl}auth`;
    const body = {email, contrasena}

    return this.http.post<AuthRespuesta>(url, body)
      .pipe(
        tap( resp => {
          if (resp.ok)
          {
            this._usuario = {
            uid: resp.uid,
            nombre: resp.nombre

            }
            
          }
          console.log(resp);
        }),
        map( resp => resp.ok ),
        catchError( err => of(false))
      )
  }
}
